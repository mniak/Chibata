﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Xunit.Ioc.Autofac;

[assembly: TestFramework("Chibata.Tests.Infrastructure.ConfigureTestFramework", "Chibata.Tests")]

namespace Chibata.Tests.Infrastructure
{
    public class ConfigureTestFramework : AutofacTestFramework
    {
        private const string TestSuffixConvention = "Tests";

        public ConfigureTestFramework(IMessageSink diagnosticMessageSink) : base(diagnosticMessageSink)
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith(TestSuffixConvention));

            builder.RegisterType<ActivityFactory>();

            Container = builder.Build();
        }
    }
}
