using System;
using System.Linq;
using Xunit;
using Xunit.Ioc.Autofac;

namespace Chibata.Tests
{
    [UseAutofacTestFramework]
    public class ActivityManagerTest
    {
        private readonly ActivityFactory activityFactory;

        public ActivityManagerTest(ActivityFactory activityFactory)
        {
            this.activityFactory = activityFactory;
        }

        [Fact]
        public void StartNew()
        {
            var title = "My activity";
            var estimate = TimeSpan.FromHours(8);
            var startMoment = DateTime.Now;

            var activity = activityFactory.StartNew(title, estimate);
            Assert.Equal(startMoment, activity.StartMoment);
            Assert.Equal(title, activity.Title);
            Assert.Equal(estimate, activity.Estimate);
            Assert.True(activity.IsActive);

            var entry = activity.TimeEntries.SingleOrDefault();
            Assert.NotNull(entry);
            Assert.Equal(startMoment, entry.Start);
        }
    }
}
