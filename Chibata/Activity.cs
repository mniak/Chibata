﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chibata
{
    public class Activity
    {
        public bool IsActive { get; }

        public void Start() { }
        public void Stop() { }

        public TimeEntryCollection TimeEntries { get; } = new TimeEntryCollection();
        public DateTime StartMoment { get; }
        public string Title { get; }
        public TimeSpan Estimate { get; }
    }
}
